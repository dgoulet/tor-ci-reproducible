## Getting Started

Simply run the `./build.sh` to start a self contained build that creates a
reproducible tarball(s) of the version(s) being released by the latest Tor CI
Release pipeline.

NOTES: This script fetches the latest version(s) being built by the CI release
pipeline meaning that the "validation" job from the "Preliminary" stage of the
pipeline must have succeeded.

Everything generated is self contained in the `build/` directory. Once it
finishes, if successful, the signature will be in
`sigs/<tor-version>/<GPG-KEYID>.asc`

The script will auto-commit the resulting signature into the repository and so
you simply need to `git push origin main` after that.

Finally, you can safely delete the `build/` once you are done.
