#!/usr/bin/env bash

function usage()
{
    echo "$(basename $0) [-h] [-k <gpg-keyid>]"
    echo
    echo "  arguments:"
    echo "   -h: show this help text"
    echo "   -k: Which GPG keyid to sign the tarballs with"
    echo
}

while getopts "hk:" opt; do
    case "$opt" in
        h) usage
            exit 0
            ;;
        k) KEYID="$OPTARG"
            shift
            OPTIND=$((OPTIND - 1))
            ;;
        *)
            echo
            usage
            exit 1
            ;;
    esac
done

BUILD="$(pwd)/build"
SIGS_DIRPATH="$(pwd)/sigs"

# This directory is needed for the build script in the CI release repository
# thus why we export it. It will be the location of the generated tarball(s).
export TARBALLS_DIR="$BUILD/tarballs"
mkdir -p $TARBALLS_DIR

# Get in the build directory and start the process.
cd $BUILD

# Get the Tor CI release repository to use to build tarball(s).
git clone https://gitlab.torproject.org/dgoulet/tor-ci-release.git
cd tor-ci-release/

# Bunch of useful functions in there.
source ./util.sh

# Fetch the version artifacts so we can learn which version and which branch
# to use to build the tarballs.
runcmd curl -L -o artifacts.zip https://gitlab.torproject.org/dgoulet/tor-ci-release/-/jobs/artifacts/main/download?job=validation
runcmd unzip -o artifacts.zip

# Export the directory containing the versions because the build tor script
# needs it to find the versions and git branches.
export VERSIONS_DIR="$(pwd)/artifacts/versions"

# That is another thing that the build script needs. The CI passes it so w
# Build all versions
./build-all-tor.sh

# Go back to our root repository
cd $BUILD/../

# For each versions, sign the generated tarball.
for file in $VERSIONS_DIR/*
do
    # Get version from file
    VERSION=$(basename $file)

    # Tarballs signature directory
    SIG_DIR="$SIGS_DIRPATH/$VERSION"
    runcmd mkdir -p $SIG_DIR

    # Maybe use a specific keyid?
    KEYID_OPT=""
    if [ ! -z "$KEYID" ]; then
        KEYID_OPT="-u $KEYID"
    fi

    # Sign the tarball.
    runcmd gpg -o $SIG_DIR/tmp.asc -ba $KEYID_OPT $TARBALLS_DIR/tor-$VERSION.tar.gz
    # Get KeyID of the signed file to identify the file.
    keyid=$(gpg --list-packets $SIG_DIR/tmp.asc | grep "keyid" | awk '{print $NF}')
    if [ -z "$keyid" ]; then
        die "Failed to extract keyID from signed file $SIG_DIR/tmp.asc. Stopping."
    fi
    SIG_PATH="$SIG_DIR/$keyid.asc"
    mv $SIG_DIR/tmp.asc $SIG_PATH

    # Add file and "intent-to-add" so it ain't staged for commit.
    runcmd git add -N $SIG_PATH
    runcmd git diff $SIG_PATH

    while true; do
        read -p "Commit (y/n)? " yn
        case $yn in
            [Yy]* ) break;;
            [Nn]* ) exit 0;;
            *) continue;;
        esac
    done

    # Add file for staging area and commit.
    runcmd git add $SIG_PATH
    runcmd git commit -s -m "$keyid: Signature for tor.git $VERSION"
done
